# LuxMed Appointment Checker

This script is adjusted to GCP Cloud Functions deployment.

## How to use

Just go to the GCP Cloud Functions and create a new function with
`check_luxmed` as the `Executed function`.

As a Trigger choose HTTP.

### Things you need adjust

`SLACK_WEBHOOK` – add your Slack Webhook
`REQUEST_URL` – add proper endpoint (see instruction below)
`LUXMED_PAYLOAD` – update with your login and password to Portal Pacjenta

### How to find out proper URL

Go to the Portal Pacjenta and try to find a new date for
the service you want to use. For example, if you want to
make an appointment with cardiologist, do the following
steps:

1. Login to the Portal Pacjenta.
2. Click on the "Umów usługę"
3. Open Developer Tools in your browser (right click -> Inspect).
4. Choose your city, service and other things and click "Szukaj".
5. Find an XHR request with endpoint containg `terms/index?`
6. Write down `cityId` and `serviceVariantId` and fill the REQUEST_URL.

## GCP Cloud Scheduler

Create a new Cloud Scheduler that will make requests periodically,
so you are sure that you availability od dates are being checked
regularly. 

For exmple let's assume you want to check every 10 minutes.

Check the URL for triggering your function.

```bash
CF_URL=$(gcloud functions describe ${CF_NAME} --region ${REGION} --format="value[](httpsTrigger.url)")
```

Create a scheduler:

```bash
gcloud scheduler jobs create http GET-pokecenterbot-${CF_NAME}" \
  --schedule="*/10 * * * *" --uri="${CF_URL}" --http-method GET
```

