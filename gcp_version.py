import requests
import json

SLACK_WEBHOOK = ""
MESSAGE_DICT = messages_dict = {
            "header": "Nowy termin do lekarza!",
            "text": """
                @channel :point_right: NOWY TERMIN!
                NOWY TERMIN! Obadaj to!
            """,
            "webhook": SLACK_WEBHOOK
        }

POST_LOGIN_URL = 'https://portalpacjenta.luxmed.pl/PatientPortal/Account/LogIn'
REQUEST_URL = 'https://portalpacjenta.luxmed.pl/PatientPortal/NewPortal/terms/nextTerms?cityId=73&serviceVariantId=4367&languageId=10&nextSearch=true&searchByMedicalSpecialist=false&isSecondServiceVariantPostTriage=false'

LUXMED_PAYLOAD = {'Login': 'PLACEHOLDER@gmail.com', 'Password': 'PLACEHOLDER'}

def create_message_payload(message_dictionary):
    """Takes dictionary with header and message text
    and constructs message payload for Slack APP.

    args:
        message_dict (dict): dictionary with required text to generate message

    return:
        message_payload (json): message to be sent on the Slack channel
    """
    message_payload =  {
        "blocks": [
            {
                "type": "header",
                "text": {
                    "type": "plain_text",
                    "text": message_dictionary["header"],
                    "emoji": True
                }
            },
            {
                "type": "section",
                "text": {
                    "type": "mrkdwn",
                    "text": message_dictionary["text"]
                }
            },
            {
                "type": "context",
                "elements": [
                    {
                        "type": "mrkdwn",
                        "text": "Trza sie leczyc, nie?!"
                    }
                ]
            }
        ]
    }
    return json.dumps(message_payload)

def post_json_message(url, payload):
    """Sends POST request with JSON data to Slack webhook
    associated with certain Slack channel.

    args:
        url (string): webhook url created in Slack api
        payload (json): message payload
    """
    headers = {'Content-type': 'application/json'}
    requests.post(url, headers=headers, data=payload)

def get_luxmed_dates():
    with requests.Session() as session:
        post = session.post(POST_LOGIN_URL, data=LUXMED_PAYLOAD)
        r = session.get(REQUEST_URL)
        json_data = json.loads(r.text)
        list_with_dates = json_data['termsForService']['termsForDays']
        return list_with_dates


def check_luxmed(request):
    possible_dates = get_luxmed_dates()
    if not possible_dates:
        print("Empty list!")
    else:
        slack_payload = create_message_payload(MESSAGE_DICT)
        post_json_message(MESSAGE_DICT["webhook"], slack_payload)
        print("We hava a possible dates!")

